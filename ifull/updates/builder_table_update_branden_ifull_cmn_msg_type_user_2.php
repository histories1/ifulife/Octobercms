<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnMsgTypeUser2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_msg_type_user', function($table)
        {
            $table->integer('cmn_msg_type_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_msg_type_user', function($table)
        {
            $table->dropColumn('cmn_msg_type_id');
        });
    }
}
