<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHousehold6 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->string('mobile', 15)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->string('mobile', 15)->nullable(false)->change();
        });
    }
}
