<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityBlockUnit extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->dropColumn('community_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->integer('community_id')->unsigned();
        });
    }
}
