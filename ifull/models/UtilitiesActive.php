<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

/**
 * Model
 */
class UtilitiesActive extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_utilities_active';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }
    
    /* ListValue */
    public $hasOne =[
        'utilities' => [
            Utilities::class,
            'key' => 'id',
            'otherKey' => 'utilities_id' ],
        'weeks' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'week' ],
        'oclocks' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'oclock' ]
    ];

    /* Dropdown */
    public function getWeekOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',7)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getOclockOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',8)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}
