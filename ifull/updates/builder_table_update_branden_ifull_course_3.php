<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourse3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->renameColumn('status', 'course_status');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->renameColumn('course_status', 'status');
        });
    }
}
