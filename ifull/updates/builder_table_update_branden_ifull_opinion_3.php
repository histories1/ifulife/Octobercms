<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinion3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->renameColumn('cmn_opinion_type_id', 'opinion_type_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->renameColumn('opinion_type_id', 'cmn_opinion_type_id');
        });
    }
}
