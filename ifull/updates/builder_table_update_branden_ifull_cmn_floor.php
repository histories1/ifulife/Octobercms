<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnFloor extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_floor', 'branden_ifull_cmn_floor');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_floor', 'branden_ifull_community_floor');
    }
}
