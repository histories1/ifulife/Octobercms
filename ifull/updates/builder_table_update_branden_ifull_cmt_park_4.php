<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtPark4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('cmn_floor_id', 'floor_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('floor_id', 'cmn_floor_id');
        });
    }
}
