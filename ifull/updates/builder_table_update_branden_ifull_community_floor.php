<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityFloor extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_floor', function($table)
        {
            $table->string('floor_code', 10);
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_floor', function($table)
        {
            $table->dropColumn('floor_code');
        });
    }
}
