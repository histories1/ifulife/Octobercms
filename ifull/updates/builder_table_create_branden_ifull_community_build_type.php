<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityBuildType extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_build_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('build_type', 20);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_build_type');
    }
}
