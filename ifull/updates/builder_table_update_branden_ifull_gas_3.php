<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullGas3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->integer('cmt_unit_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->dropColumn('cmt_unit_id');
        });
    }
}
