<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullAreaZone3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->string('zone_code', 10);
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->dropColumn('zone_code');
        });
    }
}
