<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmt4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('cmn_use_zone_id', 'use_zone_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('use_zone_id', 'cmn_use_zone_id');
        });
    }
}
