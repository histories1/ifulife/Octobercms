<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityBlockUnit5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->renameColumn('block_id', 'community_block_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->renameColumn('community_block_id', 'block_id');
        });
    }
}
