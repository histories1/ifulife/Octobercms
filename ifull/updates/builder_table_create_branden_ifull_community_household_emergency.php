<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityHouseholdEmergency extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_household_emergency', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 20);
            $table->string('relation', 30);
            $table->string('phone', 15);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_household_emergency');
    }
}
