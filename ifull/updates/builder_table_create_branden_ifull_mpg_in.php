<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullMpgIn extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_mpg_in', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->integer('community_unit_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_mpg_in');
    }
}
