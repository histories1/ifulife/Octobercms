<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtPark2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('community_block_id', 'cmt_block_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('cmt_block_id', 'community_block_id');
        });
    }
}
