<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdMember7 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->string('aws_device_id', 100)->nullable();
            $table->string('mobile', 15)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->dropColumn('aws_device_id');
            $table->dropColumn('mobile');
        });
    }
}
