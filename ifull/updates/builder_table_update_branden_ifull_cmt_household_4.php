<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHousehold4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->string('activation_code', 10)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->dropColumn('activation_code');
        });
    }
}
