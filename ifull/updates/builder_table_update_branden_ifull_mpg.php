<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpg extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_mpg_in', 'branden_ifull_mpg');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_mpg', 'branden_ifull_mpg_in');
    }
}
