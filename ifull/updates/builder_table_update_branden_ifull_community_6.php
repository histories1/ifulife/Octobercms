<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity6 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->integer('cmn_area_id')->nullable()->unsigned();
            $table->integer('cmn_zone_id')->nullable()->unsigned();
            $table->integer('cmn_use_zone_id')->nullable()->unsigned();
            $table->integer('cmn_build_type_id')->nullable()->unsigned();
            $table->integer('cmn_structure_type_id')->nullable()->unsigned();
            $table->dropColumn('area_id');
            $table->dropColumn('zone_id');
            $table->dropColumn('use_zone');
            $table->dropColumn('build_type');
            $table->dropColumn('structure_type');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->dropColumn('cmn_area_id');
            $table->dropColumn('cmn_zone_id');
            $table->dropColumn('cmn_use_zone_id');
            $table->dropColumn('cmn_build_type_id');
            $table->dropColumn('cmn_structure_type_id');
            $table->integer('area_id')->nullable()->unsigned();
            $table->integer('zone_id')->nullable()->unsigned();
            $table->string('use_zone', 10)->nullable();
            $table->string('build_type', 10)->nullable();
            $table->string('structure_type', 10)->nullable();
        });
    }
}
