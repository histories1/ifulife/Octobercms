<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilities2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->boolean('is_active');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->dropColumn('is_active');
        });
    }
}
