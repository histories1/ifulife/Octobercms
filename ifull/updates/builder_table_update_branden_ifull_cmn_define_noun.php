<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnDefineNoun extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_define_noun', function($table)
        {
            $table->integer('cmn_define_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_define_noun', function($table)
        {
            $table->dropColumn('cmn_define_id');
        });
    }
}
