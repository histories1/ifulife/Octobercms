<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdCar3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household_car', function($table)
        {
            $table->renameColumn('car_type', 'car_type_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household_car', function($table)
        {
            $table->renameColumn('car_type_id', 'car_type');
        });
    }
}
