<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsgRead extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg_read', function($table)
        {
            $table->integer('msg_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg_read', function($table)
        {
            $table->dropColumn('msg_id');
        });
    }
}
