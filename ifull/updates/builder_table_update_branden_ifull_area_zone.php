<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullAreaZone extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->string('zip_code', 10)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('zone', 30)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->text('zip_code')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('zone')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
