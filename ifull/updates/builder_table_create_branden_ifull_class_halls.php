<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullClassHalls extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_class_halls', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('class_id')->unsigned();
            $table->integer('hall')->unsigned();
            $table->dateTime('start_at');
            $table->dateTime('end_at');
            $table->string('theme', 100);
            $table->string('course_outline', 100)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_class_halls');
    }
}
