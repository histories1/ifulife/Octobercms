<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMtnVisitor extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mtn_visitor', function($table)
        {
            $table->integer('community_id')->unsigned();
            $table->integer('community_household_member_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mtn_visitor', function($table)
        {
            $table->dropColumn('community_id');
            $table->dropColumn('community_household_member_id');
        });
    }
}
