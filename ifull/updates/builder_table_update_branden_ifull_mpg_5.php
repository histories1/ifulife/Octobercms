<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpg5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->dropColumn('mpg_qty');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->smallInteger('mpg_qty')->unsigned();
        });
    }
}
