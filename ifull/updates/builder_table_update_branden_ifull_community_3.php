<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->decimal('ld_arcade_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('ld_setback_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('ld_other_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('ld_total_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('coverage_ratio', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('area_ratio', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('bd_svl_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('bd_total_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('bd_height', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('bd_arcade_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('bd_other_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('above_ars_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
            $table->decimal('under_ars_area', 10, 0)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->integer('ld_arcade_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('ld_setback_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('ld_other_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('ld_total_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('coverage_ratio')->nullable()->unsigned()->default(null)->change();
            $table->integer('area_ratio')->nullable()->unsigned()->default(null)->change();
            $table->integer('bd_svl_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('bd_total_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('bd_height')->nullable()->unsigned()->default(null)->change();
            $table->integer('bd_arcade_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('bd_other_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('above_ars_area')->nullable()->unsigned()->default(null)->change();
            $table->integer('under_ars_area')->nullable()->unsigned()->default(null)->change();
        });
    }
}
