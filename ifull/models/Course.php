<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class Course extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_course';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $hasMany =[
        'coursehalls' => [
            //'branden\ifull\models\CmtBlock',
            CourseHalls::class,
            'table' => 'branden_ifull_course_halls'//,
            //'order' => 'block'
        ]
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ],
        'coursetype' => [
            CourseType::class,
            'key' => 'id',
            'otherKey' => 'course_type_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $res = Cmt::get(['id','community'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCourseTypeIdOptions() {
        $res = CourseType::get(['id','course_type'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['course_type'];
        }
        return $ret;
    }
    public function getCourseLibraryIdOptions() {
        $res = CourseLibrary::get(['id','name'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['name'];
        }
        return $ret;
    }
    public function getEnrollmentStatusOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',5)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getCourseStatusOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',4)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getUserIdOptions() {
        $res = 'rainlab\user\models\user'::get(['id','name'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['name'];
        }
        return $ret;
    }
    /*filterFields */
    public function filterFields($fields, $context = null)
    {
        if (empty($this->course_library_id))
            return;

        $lazymsg = CourseLibrary::where('id',$this->course_library_id)
                   ->get(['id','course_type_id','name','purpose','hours',
                          'restrict','tuition','other_charge','remark']);
        foreach($lazymsg as $value) {
        $fields->course_library_id->value = $value['id'];    
        $fields->course_type_id->value    = $value['course_type_id'];
        $fields->name->value              = $value['name'];
        $fields->purpose->value           = $value['purpose'];
        $fields->hours->value             = $value['hours'];
        $fields->restrict->value          = $value['restrict'];
        $fields->tuition->value           = $value['tuition'];
        $fields->other_charge->value      = $value['other_charge'];
        $fields->remark->value            = $value['remark'];
        }
    }    
}
