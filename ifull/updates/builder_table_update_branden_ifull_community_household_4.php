<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHousehold4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->boolean('is_keep_a_pet')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->boolean('is_keep_a_pet')->default(null)->change();
        });
    }
}
