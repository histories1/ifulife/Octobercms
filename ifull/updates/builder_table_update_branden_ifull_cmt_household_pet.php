<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdPet extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_household_pet', 'branden_ifull_cmt_household_pet');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_household_pet', 'branden_ifull_community_household_pet');
    }
}
