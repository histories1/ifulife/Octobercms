<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

/**
 * Model
 */
class Opinion extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_opinion';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->setCmtIDAttribute();
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');
        }
        $this->attributes['cmt_id'] = $cmtid;
    }

    public $attachMany = [
        'images' => 'System\Models\File',
    ];
    /* Relations */
    public $hasMany =[
        'opinionreplys' => [
            //'branden\ifull\models\CmtBlock',
            OpinionReply::class,
            'table' => 'branden_ifull_opinion_reply'//,
            //'order' => 'block'
        ]
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ],
        'opiniontypes' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'opinion_type_id'],
        'opinionstatus' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'opinion_status']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            $res = Cmt::get(['id','community'])->toArray();
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            return CmtUnit::where('cmt_id',$this->cmt_id)
                   ->lists('unit','id');
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            return CmtUnit::where('cmt_id',$cmtid)
                   ->lists('unit','id');
        }
    }
    public function getCmtHouseholdIdOptions() {
        $res = CmtHousehold::where('cmt_id',$this->cmt_id)->
               get(['id','owner'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['owner'];
        }
        return $ret;
    }
    public function getCmtHouseholdMemberIdOptions() {
        $cmthouseholdid = CmtHousehold::
                          where('cmt_unit_id',$this->cmt_unit_id)
                          ->value('id');
        return CmtHouseholdMember::where('cmt_household_id',$cmthouseholdid)
               ->lists('name','id');
    }
    public function getOpinionTypeIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',10)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getOpinionStatusOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',6)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}
