<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmtBackendUsers extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_backend_users';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id']     ,
         'user' => [
            //Cmt::class,
            'Backend\Models\User'::class,
            'key' => 'id',
            'otherKey' => 'backend_users_id']                 
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $res = Cmt::get(['id','community'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    
    public function getBackendUsersIdOptions() {
        $res = 'Backend\Models\User'::get(['id','last_name','first_name','login','email'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['last_name'] . ' ' . $value['first_name'] . '   ' . 
                                 $value['login']. '   ' .$value['email'];
        }
        return $ret;
    }
}
