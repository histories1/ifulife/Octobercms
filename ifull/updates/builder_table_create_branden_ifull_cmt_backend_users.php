<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCmtBackendUsers extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_cmt_backend_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('cmt_id');
            $table->integer('backend_users_id');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_cmt_backend_users');
    }
}
