<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityPark7 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('cmn_floor_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('cmn_floor_id')->unsigned(false)->change();
        });
    }
}
