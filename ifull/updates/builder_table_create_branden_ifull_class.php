<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullClass extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_class', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('cmt_id')->unsigned();
            $table->integer('course_type_id')->unsigned();
            $table->integer('course_library_id')->unsigned();
            $table->string('name', 100);
            $table->dateTime('start_at');
            $table->string('location', 50);
            $table->integer('status')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('purpose', 100)->nullable();
            $table->string('hours')->nullable();
            $table->string('restrict', 100)->nullable();
            $table->string('tuition', 100);
            $table->string('other_charge', 100)->nullable();
            $table->text('remark')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_class');
    }
}
