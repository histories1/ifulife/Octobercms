<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHouseholdCar extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household_car', function($table)
        {
            $table->integer('community_household_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household_car', function($table)
        {
            $table->dropColumn('community_household_id');
        });
    }
}
