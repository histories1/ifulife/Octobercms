<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtBackendUsers2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_backend_users', function($table)
        {
            $table->integer('cmt_id')->unsigned()->change();
            $table->integer('backend_users_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_backend_users', function($table)
        {
            $table->integer('cmt_id')->unsigned(false)->change();
            $table->integer('backend_users_id')->unsigned(false)->change();
        });
    }
}
