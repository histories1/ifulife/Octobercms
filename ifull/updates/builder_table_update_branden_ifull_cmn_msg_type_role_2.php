<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnMsgTypeRole2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_msg_type_role', function($table)
        {
            $table->renameColumn('backend_user_role_id', 'backend_user_roles_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_msg_type_role', function($table)
        {
            $table->renameColumn('backend_user_roles_id', 'backend_user_role_id');
        });
    }
}
