<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCmnFloor extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_cmn_floor');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_cmn_floor', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('floor_code', 10);
            $table->string('floor', 20);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
