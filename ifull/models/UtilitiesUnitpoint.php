<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class UtilitiesUnitpoint extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_utilities_unitpoint';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'
        ]
        'cmt_unit_id' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'unit_id'
        ],
        'cmtholder' => [
            CmtHousehold::class,
            'key' => 'id',
            'otherKey' => 'unit_id'
        ]
        'backend_users_id' => [
            User::class,
            'key' => 'id',
            'otherKey' => 'user_id'
        ]
    ];

    /* Dropdown 定義type為dropdown類型配置的選項來源與內容 */
    public function getCmtOptions()
    {
        $res = Cmt::where('cmt_id', $this->cmt_id)->get(['id', 'community'])->toArray();
        $ret = [];
        foreach ($res as $value) {
            $ret[$value['id']] = $value['community'].'測試附加';
        }
        return $ret;
    }
}
