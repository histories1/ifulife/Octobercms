<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityBlockUnit extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_block_unit', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->integer('block_id')->unsigned();
            $table->string('unit', 10);
            $table->string('address', 50);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_block_unit');
    }
}
