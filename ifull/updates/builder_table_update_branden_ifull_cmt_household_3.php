<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHousehold3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->renameColumn('community_unit_id', 'cmt_unit_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household', function($table)
        {
            $table->renameColumn('cmt_unit_id', 'community_unit_id');
        });
    }
}
