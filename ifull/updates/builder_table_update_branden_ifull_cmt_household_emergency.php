<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdEmergency extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_household_emergency', 'branden_ifull_cmt_household_emergency');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_household_emergency', 'branden_ifull_community_household_emergency');
    }
}
