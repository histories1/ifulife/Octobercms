<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmnMsgType extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmn_msg_type';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $hasMany =[
        'cmnmsgtyperoles' => [
            //'branden\ifull\models\CmnMsgTypeRole',
            CmnMsgTypeRole::class,
            'table'  => 'branden_ifull_cmn_msg_type_role',
            'delete' => 'true',
            'order'  => 'backend_user_roles_id'
        ]
    ];
}
