<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityPark4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('cmn_floor_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->dropColumn('cmn_floor_id');
        });
    }
}
