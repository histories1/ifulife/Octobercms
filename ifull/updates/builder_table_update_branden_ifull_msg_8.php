<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsg8 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->renameColumn('cmn_msg_type_id', 'msg_type');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->renameColumn('msg_type', 'cmn_msg_type_id');
        });
    }
}
