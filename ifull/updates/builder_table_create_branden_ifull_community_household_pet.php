<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityHouseholdPet extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_household_pet', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_household_id')->unsigned();
            $table->string('name', 20);
            $table->string('breed', 30)->nullable();
            $table->string('color', 20)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_household_pet');
    }
}
