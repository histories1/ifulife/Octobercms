<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdCar extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_household_car', 'branden_ifull_cmt_household_car');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_household_car', 'branden_ifull_community_household_car');
    }
}
