<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullMpgType extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_mpg_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('mpg_type', 30);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('community_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_mpg_type');
    }
}
