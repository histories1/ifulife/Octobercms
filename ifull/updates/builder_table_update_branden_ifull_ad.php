<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullAd extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_ad', function($table)
        {
            $table->string('url', 100)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_ad', function($table)
        {
            $table->dropColumn('url');
        });
    }
}
