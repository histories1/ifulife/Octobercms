<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinion extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->integer('cmt_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->dropColumn('cmt_id');
        });
    }
}
