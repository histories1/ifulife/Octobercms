<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityUnit3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_unit', function($table)
        {
            $table->integer('cmn_floor_id')->nullable()->unsigned();
            $table->integer('cmn_area_id')->nullable()->unsigned();
            $table->integer('cmn_zone_id')->nullable()->unsigned();
            $table->dropColumn('floor_id');
            $table->dropColumn('area_id');
            $table->dropColumn('zone_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_unit', function($table)
        {
            $table->dropColumn('cmn_floor_id');
            $table->dropColumn('cmn_area_id');
            $table->dropColumn('cmn_zone_id');
            $table->integer('floor_id')->nullable()->unsigned();
            $table->integer('area_id')->nullable()->unsigned();
            $table->integer('zone_id')->nullable()->unsigned();
        });
    }
}
