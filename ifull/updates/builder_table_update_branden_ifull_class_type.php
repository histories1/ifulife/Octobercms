<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseType extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course_type', function($table)
        {
            $table->string('course_type', 60)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course_type', function($table)
        {
            $table->text('course_type')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
