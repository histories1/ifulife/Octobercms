<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilitiesActive extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities_active', function($table)
        {
            $table->integer('oclock');
            $table->boolean('is_active')->default(0);
            $table->integer('point')->nullable();
            $table->dropColumn('c00_active');
            $table->dropColumn('c00_point');
            $table->dropColumn('c01_active');
            $table->dropColumn('c01_point');
            $table->dropColumn('c02_active');
            $table->dropColumn('c02_point');
            $table->dropColumn('c03_active');
            $table->dropColumn('c03_point');
            $table->dropColumn('c04_active');
            $table->dropColumn('c04_point');
            $table->dropColumn('c05_active');
            $table->dropColumn('c05_point');
            $table->dropColumn('c06_active');
            $table->dropColumn('c06_point');
            $table->dropColumn('c07_active');
            $table->dropColumn('c07_point');
            $table->dropColumn('c08_active');
            $table->dropColumn('c08_point');
            $table->dropColumn('c09_active');
            $table->dropColumn('c09_point');
            $table->dropColumn('c10_active');
            $table->dropColumn('c10_point');
            $table->dropColumn('c11_active');
            $table->dropColumn('c11_point');
            $table->dropColumn('c12_active');
            $table->dropColumn('c12_point');
            $table->dropColumn('c13_active');
            $table->dropColumn('c13_point');
            $table->dropColumn('c14_active');
            $table->dropColumn('c14_point');
            $table->dropColumn('c15_active');
            $table->dropColumn('c15_point');
            $table->dropColumn('c16_active');
            $table->dropColumn('c16_point');
            $table->dropColumn('c17_active');
            $table->dropColumn('c17_point');
            $table->dropColumn('c18_active');
            $table->dropColumn('c18_point');
            $table->dropColumn('c19_active');
            $table->dropColumn('c19_point');
            $table->dropColumn('c20_active');
            $table->dropColumn('c20_point');
            $table->dropColumn('c21_active');
            $table->dropColumn('c21_point');
            $table->dropColumn('c22_active');
            $table->dropColumn('c22_point');
            $table->dropColumn('c23_active');
            $table->dropColumn('c23_point');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities_active', function($table)
        {
            $table->dropColumn('oclock');
            $table->dropColumn('is_active');
            $table->dropColumn('point');
            $table->boolean('c00_active')->default(0);
            $table->integer('c00_point')->nullable();
            $table->boolean('c01_active')->default(0);
            $table->integer('c01_point')->nullable();
            $table->boolean('c02_active')->default(0);
            $table->integer('c02_point')->nullable();
            $table->boolean('c03_active')->default(0);
            $table->integer('c03_point')->nullable();
            $table->boolean('c04_active')->default(0);
            $table->integer('c04_point')->nullable();
            $table->boolean('c05_active')->default(0);
            $table->integer('c05_point')->nullable();
            $table->boolean('c06_active')->default(0);
            $table->integer('c06_point')->nullable();
            $table->boolean('c07_active')->default(0);
            $table->integer('c07_point')->nullable();
            $table->boolean('c08_active')->default(0);
            $table->integer('c08_point')->nullable();
            $table->boolean('c09_active')->default(0);
            $table->integer('c09_point')->nullable();
            $table->boolean('c10_active')->default(0);
            $table->integer('c10_point')->nullable();
            $table->boolean('c11_active')->default(0);
            $table->integer('c11_point')->nullable();
            $table->boolean('c12_active')->default(0);
            $table->integer('c12_point')->nullable();
            $table->boolean('c13_active')->default(0);
            $table->integer('c13_point')->nullable();
            $table->boolean('c14_active')->default(0);
            $table->integer('c14_point')->nullable();
            $table->boolean('c15_active')->default(0);
            $table->integer('c15_point')->nullable();
            $table->boolean('c16_active')->default(0);
            $table->integer('c16_point')->nullable();
            $table->boolean('c17_active')->default(0);
            $table->integer('c17_point')->nullable();
            $table->boolean('c18_active')->default(0);
            $table->integer('c18_point')->nullable();
            $table->boolean('c19_active')->default(0);
            $table->integer('c19_point')->nullable();
            $table->boolean('c20_active')->default(0);
            $table->integer('c20_point')->nullable();
            $table->boolean('c21_active')->default(0);
            $table->integer('c21_point')->nullable();
            $table->boolean('c22_active')->default(0);
            $table->integer('c22_point')->nullable();
            $table->boolean('c23_active')->default(0);
            $table->integer('c23_point')->nullable();
        });
    }
}
