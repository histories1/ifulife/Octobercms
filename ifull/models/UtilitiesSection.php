<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class UtilitiesSection extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_utilities_section';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'utilities' => [
            Utilities::class,
            'key' => 'id',
            'otherKey' => 'utilities_id'
        ]
    ];

}
