<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnUseZone extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_use_zone', 'branden_ifull_cmn_use_zone');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_use_zone', 'branden_ifull_community_use_zone');
    }
}
