<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnArea extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_area', 'branden_ifull_cmn_area');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_area', 'branden_ifull_area');
    }
}
