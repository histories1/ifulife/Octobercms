<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseHalls extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_class_halls', 'branden_ifull_course_halls');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_course_halls', 'branden_ifull_class_halls');
    }
}
