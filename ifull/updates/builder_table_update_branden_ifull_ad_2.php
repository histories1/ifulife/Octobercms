<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullAd2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_ad', function($table)
        {
            $table->dateTime('test');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_ad', function($table)
        {
            $table->dropColumn('test');
        });
    }
}
