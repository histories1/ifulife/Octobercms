<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHousehold extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_household', 'branden_ifull_cmt_household');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_household', 'branden_ifull_community_household');
    }
}
