<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CourseLibrary extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_course_library';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ],
        'coursetype' => [
            CourseType::class,
            'key' => 'id',
            'otherKey' => 'course_type_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $res = Cmt::get(['id','community'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCourseTypeIdOptions() {
        $res = CourseType::get(['id','course_type'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['course_type'];
        }
        return $ret;
    }
}
