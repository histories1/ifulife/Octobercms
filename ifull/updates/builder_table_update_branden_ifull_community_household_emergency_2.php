<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHouseholdEmergency2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household_emergency', function($table)
        {
            $table->renameColumn('community_household_id', 'cmt_household_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household_emergency', function($table)
        {
            $table->renameColumn('cmt_household_id', 'community_household_id');
        });
    }
}
