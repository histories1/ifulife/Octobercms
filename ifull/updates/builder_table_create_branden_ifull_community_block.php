<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityBlock extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_block', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->string('block', 10);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_block');
    }
}
