<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtPark extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_park', 'branden_ifull_cmt_park');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_park', 'branden_ifull_community_park');
    }
}
