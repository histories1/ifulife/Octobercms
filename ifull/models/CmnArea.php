<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmnArea extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmn_area';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $hasMany =[
        'cmnareazones' => [
            //'branden\ifull\models\CmnAreaZone',
            CmnAreaZone::class,
            'table'  => 'branden_ifull_area_zone',
            'delete' => 'true',
            'order'  => 'zone_code'
        ]
    ];
}
