<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtPark5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->integer('cmt_household_member_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->dropColumn('cmt_household_member_id');
        });
    }
}
