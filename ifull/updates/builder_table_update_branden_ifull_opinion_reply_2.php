<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinionReply2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion_reply', function($table)
        {
            $table->integer('backend_users_id')->unsigned()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion_reply', function($table)
        {
            $table->integer('backend_users_id')->unsigned(false)->change();
        });
    }
}
