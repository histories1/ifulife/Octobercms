<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use DB;
use DateTime;

class Mpg extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Branden.iFull', 'main-menu-item6', 'side-menu-item3');
    }

    /*設定使用者可操作之社區*/
    public function listExtendQuery($query, $definition = null)
    {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            $cmtid  = DB::table('branden_ifull_cmt')
                      ->lists('id');
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
        }
        //$cmtid = 'Branden\iFull\Models\CmtBackendUsers'::get(['cmt_id'])->toArray();
        $query->wherein('cmt_id', $cmtid);
    }

    public function filterFields($fields, $context = null)
    {
        if (empty($this->rcp_cmt_household_member_id))
            return;

        // do something to get the name value based on the code
        //$newName = '';
        $fields->collar_at->value = now;

    }
    public function render()
    {
        $this->vars['cmt_unit_id'] = $this->cmt_unit_id;

        return $this->makePartial('update');
    }

    public function onBatchCollarForm()
    {
        try {
            $this->vars['checked'] = post('checked');
        }
        catch (Exception $ex) {
            $this->handleError($ex);
        }

        return $this->makePartial('batch_collar_form');
    }
}
