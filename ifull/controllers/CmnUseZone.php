<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CmnUseZone extends Controller
{
    public $implement = [        
        'Backend\Behaviors\ListController',        
        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'iFull.cmn.UseZone' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Branden.iFull', 'main-menu-item', 'side-menu-item2');
    }
}
