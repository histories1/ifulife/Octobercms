<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CourseHalls extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_course_halls';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
