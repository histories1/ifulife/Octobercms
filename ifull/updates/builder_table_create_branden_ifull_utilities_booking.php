<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullUtilitiesBooking extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_utilities_booking', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('utilities_id')->unsigned();
            $table->integer('cmt_household_member_id')->unsigned();
            $table->date('booking_date');
            $table->time('b_time');
            $table->time('e_time');
            $table->integer('point')->nullable()->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_utilities_booking');
    }
}
