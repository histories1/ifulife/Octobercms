<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class UtilitiesBookingOclock extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_utilities_booking_oclock';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
