<?php namespace Branden\Ifull\Components;

use Cms\Classes\ComponentBase;
use Db;

class Msgs extends ComponentBase
{
    public function componentDetails(){
    	return [
            'name' => 'Msg list',
            'description' => 'List of Msgs'
    	];
    }

    public function onRun(){
    	$this->msgs = $this->loadMsgs();
    }

    protected function loadMsgs(){
        $query = Db::table('branden_ifull_msg')
                   ->join('branden_ifull_cmn_msg_type', 'cmn_msg_type_id', '=', 'branden_ifull_cmn_msg_type.id')
                   ->join('branden_ifull_cmt'         , 'cmt_id'         , '=', 'branden_ifull_cmt.id')
                   ->select('branden_ifull_msg.*', 'branden_ifull_cmn_msg_type.msg_type', 'branden_ifull_cmt.community')
                   -> get(); 

    	return $query;

    }

    public $msgs;

}