<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsg extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dateTime('start_at');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dropColumn('start_at');
        });
    }
}
