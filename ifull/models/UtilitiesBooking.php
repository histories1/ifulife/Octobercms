<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class UtilitiesBooking extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_utilities_booking';

    use Nullable;

    public $nullable = [
        'cmt_id',  
        'cmt_unit_id',   
        'cmt_household_member_id', 
        'backend_users_id',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }
    /* Relations */
    public $hasMany =[
        'utilitiesbookingoclocks' => [
            //'branden\ifull\models\CmtBlock',
            UtilitiesBookingOclock::class,
            'table' => 'branden_ifull_utilities_booking_oclock'//,
            //'order' => 'block'
        ]
    ];
   /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtunit' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'cmt_unit_id'],
        'cmthouseholdmember' => [
            CmtHouseholdMember::class,
            'key' => 'id',
            'otherKey' => 'cmt_household_member_id'],
        'utilities' => [
            Utilities::class,
            'key' => 'id',
            'otherKey' => 'utilities_id']        
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $res = Cmt::get(['id','community'])->toArray();
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }     
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {  
            return CmtUnit::where('cmt_id',$this->cmt_id)
                   ->lists('unit','id');
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            return CmtUnit::where('cmt_id',$cmtid)
                   ->lists('unit','id');
        }
    }
/*    
    public function getCmtUnitIdOptions() {
        $res = CmtUnit::where('cmt_id',$this->cmt_id)->
               get(['id','unit'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['unit'];
        }
        return $ret;
    }
*/
    public function getCmtHouseholdMemberIdOptions() {
        $cmthouseholdid = CmtHousehold::
                          where('cmt_unit_id',$this->cmt_unit_id)
                          ->value('id');                 
        return CmtHouseholdMember::where('cmt_household_id',$cmthouseholdid)
               ->lists('name','id');
    }
    public function getUtilitiesIdOptions() {
        return Utilities::where('cmt_id',$this->cmt_id)
               ->lists('utilities','id');
    }
    public function getBackendUsersIdOptions() {
        $res = 'Backend\Models\User'::get(['id','last_name','first_name','login','email'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['last_name'] . ' ' . $value['first_name'] . '   ' . 
                                 $value['login']. '   ' .$value['email'];
        }
        return $ret;
    }
}

