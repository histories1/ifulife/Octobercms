<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullUtilities extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_utilities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('utilities', 50);
            $table->time('open_at')->nullable();
            $table->time('close_at')->nullable();
            $table->integer('capacity');
            $table->text('description')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_utilities');
    }
}
