<?php namespace Branden\iFull\Models;

use Model;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class Cmt extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt';

    use Nullable;

    public $nullable = [
        'cmn_area_id',    // Define which fields should be inserted as NULL when 
        'cmn_zone_id',
        'sponsor_area_id', 
        'sponsor_zone_id', 
        'use_zone_id',
        'build_type_id',
        'cmn_structure_type_id',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* Relations */
    public $hasMany =[
        'cmtblocks' => [
            //'branden\ifull\models\CmtBlock',
            CmtBlock::class,
            'table' => 'branden_ifull_cmt_block'//,
            //'order' => 'block'
        ],
        'cmtunits' => [
            //'branden\ifull\models\CmtUnit',
            CmtUnit::class,
            'table' => 'branden_ifull_cmt_unit'//,
            //'order' => 'block'
        ],
        'cmtparks' => [
            //'branden\ifull\models\CmtPark',
            CmtPark::class,
            'table' => 'branden_ifull_cmt_park'//,
            //'order' => 'block'
        ]
    ];

    /* ListValue */
    public $hasOne =[
        'cmtarea' => [
            CmnArea::class,
            'key' => 'id',
            'otherKey' => 'cmn_area_id'],
        'cmtareazone' => [
            CmnAreaZone::class,
            'key' => 'id',
            'otherKey' => 'cmn_zone_id']
    ];

    /* Dropdown */
    public function getSponsorAreaIdOptions() {
        $res = CmnArea::get(['id','area'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['area'];
        }
        return $ret;
    }

    public function getSponsorZoneIdOptions() {
        $res = CmnAreaZone::where('cmn_area_id',$this->sponsor_area_id)->get(['id','zip_code','zone'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['zip_code'].' '.$value['zone'];
        }
        return $ret;
    }

    public function getCmnAreaIdOptions() {
        $res = CmnArea::get(['id','area'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['area'];
        }
        return $ret;
    }

    public function getCmnZoneIdOptions() {
        $res = CmnAreaZone::where('cmn_area_id',$this->cmn_area_id)->get(['id','zip_code','zone'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['zip_code'].' '.$value['zone'];
        }
        return $ret;
    }

    public function getBuildTypeIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',13)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }

    public function getUseZoneIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',12)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }

}
