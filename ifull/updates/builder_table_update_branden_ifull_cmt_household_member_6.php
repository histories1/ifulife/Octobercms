<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdMember6 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->string('sex', 1)->nullable();
            $table->date('birthday')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->dropColumn('sex');
            $table->dropColumn('birthday');
        });
    }
}
