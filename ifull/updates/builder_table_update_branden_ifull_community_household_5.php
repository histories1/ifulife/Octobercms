<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHousehold5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->renameColumn('living_status_id', 'cmn_living_status_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->renameColumn('cmn_living_status_id', 'living_status_id');
        });
    }
}
