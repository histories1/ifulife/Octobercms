<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCmnDefineNoun extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_cmn_define_noun', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('noun_code', 10);
            $table->string('noun', 30);
            $table->string('para01', 20)->nullable();
            $table->string('para02', 20)->nullable();
            $table->string('para03', 20)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_cmn_define_noun');
    }
}
