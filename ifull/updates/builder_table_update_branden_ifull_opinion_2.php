<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinion2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->integer('cmt_household_id')->nullable();
            $table->integer('cmt_household_member_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->dropColumn('cmt_household_id');
            $table->dropColumn('cmt_household_member_id');
        });
    }
}
