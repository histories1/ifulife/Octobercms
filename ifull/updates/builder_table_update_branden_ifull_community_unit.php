<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityUnit extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_block_unit', 'branden_ifull_community_unit');
        Schema::table('branden_ifull_community_unit', function($table)
        {
            $table->renameColumn('community_block_id', 'community_id');
        });
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_community_unit', 'branden_ifull_community_block_unit');
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->renameColumn('community_id', 'community_block_id');
        });
    }
}
