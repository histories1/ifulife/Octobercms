<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityPark8 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->renameColumn('community_id', 'cmt_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->renameColumn('cmt_id', 'community_id');
        });
    }
}
