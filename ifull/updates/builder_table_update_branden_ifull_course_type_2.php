<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseType2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course_type', function($table)
        {
            $table->renameColumn('class_type', 'course_type');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course_type', function($table)
        {
            $table->renameColumn('course_type', 'class_type');
        });
    }
}
