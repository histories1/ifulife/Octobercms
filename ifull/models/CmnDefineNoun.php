<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmnDefineNoun extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmn_define_noun';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
