<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullOpinionReply extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_opinion_reply', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('opinion_id')->unsigned();
            $table->dateTime('reply_date');
            $table->text('reply');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_opinion_reply');
    }
}
