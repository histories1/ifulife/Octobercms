<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpg2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->integer('cmt_id')->unsigned();
            $table->integer('cmt_unit_id')->unsigned();
            $table->integer('cmt_household_member_id')->unsigned();
            $table->integer('mpg_type_id')->unsigned();
            $table->smallInteger('mpg_qty')->unsigned();
            $table->string('barcode', 30)->nullable();
            $table->integer('mpg_location_id')->nullable()->unsigned();
            $table->dateTime('receipt_at');
            $table->dateTime('collar_at')->nullable();
            $table->string('mpgcode', 20)->nullable();
            $table->integer('rcp_cmt_household_member_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->dropColumn('community_id');
            $table->dropColumn('community_unit_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->dropColumn('cmt_id');
            $table->dropColumn('cmt_unit_id');
            $table->dropColumn('cmt_household_member_id');
            $table->dropColumn('mpg_type_id');
            $table->dropColumn('mpg_qty');
            $table->dropColumn('barcode');
            $table->dropColumn('mpg_location_id');
            $table->dropColumn('receipt_at');
            $table->dropColumn('collar_at');
            $table->dropColumn('mpgcode');
            $table->dropColumn('rcp_cmt_household_member_id');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->integer('community_id')->unsigned();
            $table->integer('community_unit_id')->unsigned();
        });
    }
}
