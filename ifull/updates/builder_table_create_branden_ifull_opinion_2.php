<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullOpinion2 extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_opinion', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('cmt_id')->unsigned();
            $table->integer('cmt_household_id')->nullable()->unsigned();
            $table->integer('cmt_household_member_id')->unsigned();
            $table->string('opinion_no', 10);
            $table->integer('cmn_opinion_type_id')->unsigned();
            $table->dateTime('opinion_date');
            $table->text('opinion');
            $table->integer('opinion_status')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_opinion');
    }
}
