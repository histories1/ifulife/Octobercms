<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnMsgTypeRole extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_cmn_msg_type_user', 'branden_ifull_cmn_msg_type_role');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_msg_type_role', 'branden_ifull_cmn_msg_type_user');
    }
}
