<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullAreaZone2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->integer('area_id')->nullable(false)->unsigned()->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->smallInteger('area_id')->nullable(false)->unsigned()->default(null)->change();
        });
    }
}
