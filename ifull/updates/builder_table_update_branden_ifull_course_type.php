<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseType extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_course_type', 'branden_ifull_course_type');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_course_type', 'branden_ifull_course_type');
    }
}
