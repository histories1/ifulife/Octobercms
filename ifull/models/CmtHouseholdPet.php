<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmtHouseholdPet extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_household_pet';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
