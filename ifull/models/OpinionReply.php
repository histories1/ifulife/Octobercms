<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;
use DateTime;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class OpinionReply extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_opinion_reply';

    use Nullable;

    public $nullable = [
        'quick_reply',  
        'backend_users_id', 
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'backendusers' => [
            'Backend\Models\User',
            'key' => 'id',
            'otherKey' => 'backend_users_id' ],
    ];
    /* Dropdown */
    public function getBackendUsersIdOptions() {
        $res = 'Backend\Models\User'::get(['id','last_name','first_name','login','email'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['last_name'] . ' ' . $value['first_name'] . '   ' . 
                                 $value['login']. '   ' .$value['email'];
        }
        return $ret;
    }
    public function getQuickReplyOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',16)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    /*filterFields */
    public function filterFields($fields, $context = null)
    {
        if (empty($this->quick_reply) && empty($this->backend_users_id))
            return;

        $reply = CmnDefineNoun::where('id',$this->quick_reply)
                   ->get(['noun']);
        foreach($reply as $value) {
            $fields->reply->value           = $value['noun'];
            $datetime = new DateTime('now');
            $fields->reply_date->value = $datetime;
        }
        $datetime = new DateTime('now');
        $fields->reply_date->value = $datetime;
    }

}
