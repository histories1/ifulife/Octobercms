<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityBlockUnit3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->string('address', 50)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->string('address', 50)->nullable(false)->change();
        });
    }
}
