<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmt extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community', 'branden_ifull_cmt');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt', 'branden_ifull_community');
    }
}
