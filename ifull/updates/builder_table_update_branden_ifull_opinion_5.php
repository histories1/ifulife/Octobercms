<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinion5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->integer('cmt_unit_id')->nullable()->unsigned();
            $table->integer('cmt_household_member_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->dropColumn('cmt_unit_id');
            $table->integer('cmt_household_member_id')->nullable(false)->change();
        });
    }
}
