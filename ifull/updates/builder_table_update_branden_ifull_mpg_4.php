<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpg4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->integer('rcp_cmt_household_member_id')->nullable()->change();
            $table->text('remark')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->integer('rcp_cmt_household_member_id')->nullable(false)->change();
            $table->text('remark')->nullable(false)->change();
        });
    }
}
