<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmtHouseholdCar extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_household_car';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    /* ListValue */
    public $hasOne =[
        'cartypes' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'car_type_id'],            
    ];
    /* Dropdown */
    public function getCarTypeIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',1)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}