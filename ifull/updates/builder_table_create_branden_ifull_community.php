<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunity extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('community', 30);
            $table->string('license_no', 50)->nullable();
            $table->string('sponsor', 50)->nullable();
            $table->string('sponsor_chairman', 20)->nullable();
            $table->integer('sponsor_area_id')->nullable()->unsigned();
            $table->integer('sponsor_zone_id')->nullable()->unsigned();
            $table->string('sponsor_address', 50)->nullable();
            $table->string('designer', 20)->nullable();
            $table->string('designer_company', 50)->nullable();
            $table->string('supervisor', 20)->nullable();
            $table->string('supervisor_company', 50)->nullable();
            $table->string('builder', 20)->nullable();
            $table->string('builder_company', 50)->nullable();
            $table->integer('area_id')->nullable()->unsigned();
            $table->integer('zone_id')->nullable()->unsigned();
            $table->string('land_no', 50)->nullable();
            $table->string('address', 50)->nullable();
            $table->string('use_zone', 10)->nullable();
            $table->integer('ld_arcade_area')->nullable()->unsigned();
            $table->integer('ld_setback_area')->nullable()->unsigned();
            $table->integer('ld_other_area')->nullable()->unsigned();
            $table->integer('ld_total_area')->nullable()->unsigned();
            $table->smallInteger('above_floor')->nullable()->unsigned();
            $table->smallInteger('under_floor')->nullable()->unsigned();
            $table->smallInteger('buildings')->nullable()->unsigned();
            $table->smallInteger('blocks')->nullable()->unsigned();
            $table->smallInteger('units')->nullable()->unsigned();
            $table->integer('coverage_ratio')->nullable()->unsigned();
            $table->integer('area_ratio')->nullable()->unsigned();
            $table->string('build_type', 10)->nullable();
            $table->integer('bd_svl_area')->nullable()->unsigned();
            $table->integer('bd_total_area')->nullable()->unsigned();
            $table->integer('bd_height')->nullable()->unsigned();
            $table->string('structure_type', 10)->nullable();
            $table->integer('bd_arcade_area')->nullable()->unsigned();
            $table->integer('bd_other_area')->nullable()->unsigned();
            $table->integer('above_ars_area')->nullable()->unsigned();
            $table->integer('under_ars_area')->nullable()->unsigned();
            $table->smallInteger('in_above_park')->nullable()->unsigned();
            $table->smallInteger('in_under_park')->nullable()->unsigned();
            $table->smallInteger('out_park')->nullable()->unsigned();
            $table->smallInteger('in_above_reward_park')->nullable()->unsigned();
            $table->smallInteger('in_under_reward_park')->nullable()->unsigned();
            $table->smallInteger('out_reward_park')->nullable()->unsigned();
            $table->smallInteger('legal_park')->nullable()->unsigned();
            $table->smallInteger('actual_park')->nullable()->unsigned();
            $table->date('license_date')->nullable();
            $table->date('bd_license_date')->nullable();
            $table->string('bd_license_no', 50)->nullable();
            $table->date('bd_start_date')->nullable();
            $table->date('bd_complete_date')->nullable();
            $table->string('public_building', 50)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community');
    }
}
