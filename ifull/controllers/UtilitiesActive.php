<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use DB;

class UtilitiesActive extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Branden.iFull', 'main-menu-item7', 'side-menu-item2');
    }
}
