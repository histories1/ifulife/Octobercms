<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullCommunityHousehold extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_community_household', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_id')->unsigned();
            $table->integer('community_unit_id')->unsigned();
            $table->date('in_date');
            $table->date('out_date')->nullable();
            $table->string('owner', 50);
            $table->string('sex', 1)->nullable();
            $table->date('birthday')->nullable();
            $table->integer('reg_area_id')->nullable()->unsigned();
            $table->integer('reg_zone_id')->nullable()->unsigned();
            $table->string('reg_address', 50)->nullable();
            $table->integer('con_area_id')->unsigned();
            $table->integer('con_zone_id')->unsigned();
            $table->string('con_address', 50);
            $table->string('phone', 15)->nullable();
            $table->string('mobile', 15);
            $table->string('email', 30)->nullable();
            $table->integer('living_status_id')->nullable()->unsigned();
            $table->string('living_status', 50)->nullable();
            $table->string('rent_no', 10)->nullable();
            $table->boolean('is_keep_a_pet');
            $table->string('locker', 20)->nullable();
            $table->text('remark')->nullable();
            $table->string('agent', 20)->nullable();
            $table->string('agent_phone', 15)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_community_household');
    }
}
