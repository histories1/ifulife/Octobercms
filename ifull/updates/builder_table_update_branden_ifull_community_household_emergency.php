<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHouseholdEmergency extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household_emergency', function($table)
        {
            $table->integer('community_household_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household_emergency', function($table)
        {
            $table->dropColumn('community_household_id');
        });
    }
}
