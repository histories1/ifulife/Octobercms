<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class ShareparkStatus extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_househlod_sharepark_status';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
