<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCommunityHouseholdPark extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_community_household_park');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_community_household_park', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('community_park_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
