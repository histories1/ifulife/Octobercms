<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsgLazy2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg_lazy', function($table)
        {
            $table->string('title', 150)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg_lazy', function($table)
        {
            $table->string('title', 120)->change();
        });
    }
}
