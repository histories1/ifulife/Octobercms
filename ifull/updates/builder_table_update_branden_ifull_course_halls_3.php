<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseHalls3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course_halls', function($table)
        {
            $table->string('hall', 30)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course_halls', function($table)
        {
            $table->integer('hall')->nullable(false)->unsigned()->default(null)->change();
        });
    }
}
