<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseLibrary extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_class_library', 'branden_ifull_course_library');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_course_library', 'branden_ifull_class_library');
    }
}
