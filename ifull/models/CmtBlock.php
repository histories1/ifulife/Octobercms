<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

/**
 * Model
 */
class CmtBlock extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_block';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }
    /* Relations */
    public $hasMany =[
        'cmtunits' => [
            //'branden\ifull\models\CmtUnit',
            CmtUnit::class,
            'table' => 'branden_ifull_cmt_unit'//,
            //'order' => 'block'
        ],
        'cmtparks' => [
            //'branden\ifull\models\CmtPark',
            CmtPark::class,
            'table' => 'branden_ifull_cmt_park'//,
            //'order' => 'block'
        ]
    ];

    //public $belongsTo = [
    //    'cmt' => ['branden\ifull\models\Cmt']
    //];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ]
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $res = Cmt::get(['id','community'])->toArray();
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }     
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
}
