<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityPark6 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('community_unit_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->dropColumn('community_unit_id');
        });
    }
}
