<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use DB;

class MsgLazy extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Branden.iFull', 'main-menu-item5', 'side-menu-item');
    }

    /*設定使用者可操作之社區*/
    public function listExtendQuery($query, $definition = null)
    {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $cmtid  = DB::table('branden_ifull_cmt')
                      ->lists('id');
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
        }              
        //$cmtid = 'Branden\iFull\Models\CmtBackendUsers'::get(['cmt_id'])->toArray();
        $query->wherein('cmt_id', $cmtid);       
    }
}
