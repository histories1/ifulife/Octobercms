<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullGas4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->date('year_month');
            $table->dropColumn('year');
            $table->dropColumn('month');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->dropColumn('year_month');
            $table->integer('year')->unsigned();
            $table->integer('month')->unsigned();
        });
    }
}
