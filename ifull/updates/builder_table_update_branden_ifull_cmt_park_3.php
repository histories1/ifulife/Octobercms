<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtPark3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('cmn_park_type_id', 'park_type_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_park', function($table)
        {
            $table->renameColumn('park_type_id', 'cmn_park_type_id');
        });
    }
}
