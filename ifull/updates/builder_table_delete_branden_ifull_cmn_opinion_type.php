<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCmnOpinionType extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_cmn_opinion_type');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_cmn_opinion_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('opinion_type', 50);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
