<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullClassLibrary extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_class_library', function($table)
        {
            $table->renameColumn('type_id', 'course_type_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course_library', function($table)
        {
            $table->renameColumn('course_type_id', 'type_id');
        });
    }
}
