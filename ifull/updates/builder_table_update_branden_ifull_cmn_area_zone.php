<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnAreaZone extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_area_zone', 'branden_ifull_cmn_area_zone');
        Schema::table('branden_ifull_cmn_area_zone', function($table)
        {
            $table->renameColumn('area_id', 'cmn_area_id');
        });
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_area_zone', 'branden_ifull_area_zone');
        Schema::table('branden_ifull_area_zone', function($table)
        {
            $table->renameColumn('cmn_area_id', 'area_id');
        });
    }
}
