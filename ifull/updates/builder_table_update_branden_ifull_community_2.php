<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->text('remark')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->dropColumn('remark');
        });
    }
}
