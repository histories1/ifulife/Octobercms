<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityUnit2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_unit', function($table)
        {
            $table->integer('community_block_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_unit', function($table)
        {
            $table->dropColumn('community_block_id');
        });
    }
}
