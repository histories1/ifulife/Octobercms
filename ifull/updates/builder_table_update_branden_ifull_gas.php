<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullGas extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->renameColumn('degrss', 'degree');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->renameColumn('degree', 'degrss');
        });
    }
}
