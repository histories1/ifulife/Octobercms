<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmt5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('cmn_build_type_id', 'build_type_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('build_type_id', 'cmn_build_type_id');
        });
    }
}
