<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class CmtPark extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_park';

    use Nullable;

    public $nullable = [
        'cmt_block_id',   
        'park_type_id',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];


    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }
    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtblock' => [
            CmtBlock::class,
            'key' => 'id',
            'otherKey' => 'cmt_block_id'],
        'cmtunit' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'cmt_unit_id'],
        'floor' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'floor_id'],
        'parktype' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'park_type_id']
        
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $res = Cmt::get(['id','community'])->toArray();
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }     
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $res = CmtUnit::where('cmt_id',$this->cmt_id)->
               get(['id','unit'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['unit'];
        }
        return $ret;
    }
    public function getCmtBlockIdOptions() {
        $res = CmtBlock::where('cmt_id',$this->cmt_id)->get(['id','block'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['block'];
        }
        return $ret;
    }
    public function getFloorIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',15)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getParkTypeIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',14)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}
