<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityHousehold extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->date('closing_date')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_household', function($table)
        {
            $table->dropColumn('closing_date');
        });
    }
}
