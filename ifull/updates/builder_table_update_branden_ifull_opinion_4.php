<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinion4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->string('phone_model', 30)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion', function($table)
        {
            $table->dropColumn('phone_model');
        });
    }
}
