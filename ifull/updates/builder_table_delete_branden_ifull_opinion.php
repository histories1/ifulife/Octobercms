<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullOpinion extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_opinion');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_opinion', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('cmt_id')->unsigned();
            $table->string('opinion_no', 10);
            $table->integer('cmn_opinion_type_id')->unsigned();
            $table->dateTime('opinion_date');
            $table->text('opinion');
            $table->integer('opinion_status')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('cmt_household_id')->nullable();
            $table->integer('cmt_household_member_id')->nullable();
        });
    }
}
