<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourseHalls2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course_halls', function($table)
        {
            $table->renameColumn('class_id', 'course_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course_halls', function($table)
        {
            $table->renameColumn('course_id', 'class_id');
        });
    }
}
