<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;
use DateTime;

use October\Rain\Database\Traits\Nullable;

/**
 * Model
 */
class Msg extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_msg';

    use Nullable;

    public $nullable = [
        'msg_lazy_id',    // Define which fields should be inserted as NULL when
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->setCmtIDAttribute();
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');
        }
        $this->attributes['cmt_id'] = $cmtid;
    }

    /* Relations */
    public $hasMany =[
        'msgblocks' => [
            //'branden\ifull\models\CmtBlock',
            MsgBlock::class,
            'table' => 'branden_ifull_msg_block'//,
            //'order' => 'block'
        ],
        'msgunits' => [
            //'branden\ifull\models\CmtUnit',
            MsgUnit::class,
            'table' => 'branden_ifull_msg_unit'//,
            //'order' => 'block'
        ]
    ];
    public $attachMany = [
        'images' => 'System\Models\File',
        'files'  => 'System\Models\File'
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ],
        'cmnmsgtype' => [
            CmnMsgType::class,
            'key' => 'id',
            'otherKey' => 'cmn_msg_type_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            $res = Cmt::get(['id','community'])->toArray();
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmnMsgTypeIdOptions() {
        $res = CmnMsgType::get(['id','msg_type'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['msg_type'];
        }
        return $ret;
    }
    public function getMsgLazyIdOptions() {
        $res = MsgLazy::get(['id','title','message'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['title'];
            //$ret[$value['id']] = $value['title'] . ' ' . $value['message'];
        }
        return $ret;
    }
    /*filterFields */
    public function filterFields($fields, $context = null)
    {
        if (empty($this->msg_lazy_id) && empty($this->cmn_msg_type_id))
            return;

        $lazymsg = MsgLazy::where('id',$this->msg_lazy_id)
                   ->get(['cmn_msg_type_id','title','message']);
        foreach($lazymsg as $value) {
            $fields->cmn_msg_type_id->value = $value['cmn_msg_type_id'];
            $fields->title->value           = $value['title'];
            $fields->message->value         = $value['message'];
        }
        $datetime = new DateTime('now');
        $fields->start_at->value = $datetime;
    }

}
