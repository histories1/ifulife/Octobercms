<?php namespace Branden\iFull\Models;

use Model;
use Backend\Models\UserRole;
/**
 * Model
 */
class CmnMsgTypeRole extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmn_msg_type_role';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'userrole' => [
            UserRole::class,
            'key' => 'id',
            'otherKey' => 'backend_user_roles_id']
    ];

    /* Dropdown */
    public function getBackendUserRolesIdOptions() {
        $res = UserRole::get(['id','name'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['name'];
        }
        return $ret;
    }
    
}
