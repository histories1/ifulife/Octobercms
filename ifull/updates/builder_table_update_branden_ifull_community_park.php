<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityPark extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('park_type_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park', function($table)
        {
            $table->integer('park_type_id')->nullable(false)->change();
        });
    }
}
