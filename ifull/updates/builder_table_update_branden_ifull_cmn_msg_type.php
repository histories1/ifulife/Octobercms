<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnMsgType extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_msg_type', function($table)
        {
            $table->string('msg_type', 50)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_msg_type', function($table)
        {
            $table->string('msg_type', 191)->change();
        });
    }
}
