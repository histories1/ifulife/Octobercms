<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnParkType extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_park_type', 'branden_ifull_cmn_park_type');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_park_type', 'branden_ifull_community_park_type');
    }
}
