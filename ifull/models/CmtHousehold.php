<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class CmtHousehold extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_household';

    use Nullable;

    public $nullable = [
        'reg_area_id',    // Define which fields should be inserted as NULL when 
        'reg_zone_id',
        'con_area_id', 
        'con_zone_id',
        'living_status_id',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }

    /* Relations */
    public $hasMany =[
        'cmthouseholdmembers' => [
            //'branden\ifull\models\CmtBlock',
            CmtHouseholdMember::class,
            'table' => 'branden_ifull_cmt_household_member'//,
            //'order' => 'block'
        ],
        'cmthouseholdpets' => [
            //'branden\ifull\models\CmtBlock',
            CmtHouseholdPet::class,
            'table' => 'branden_ifull_cmt_household_pet'//,
            //'order' => 'block'
        ],
        'cmthouseholdemergencys' => [
            //'branden\ifull\models\CmtBlock',
            CmtHouseholdEmergency::class,
            'table' => 'branden_ifull_cmt_household_emergency'//,
            //'order' => 'block'
        ],
        'cmthouseholdcars' => [
            //'branden\ifull\models\CmtBlock',
            CmtHouseholdCar::class,
            'table' => 'branden_ifull_cmt_household_car'//,
            //'order' => 'block'
        ]
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtunit' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'cmt_unit_id'],
        'livingstatus' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'living_status_id'],            
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $res = Cmt::get(['id','community'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $res = CmtUnit::where('cmt_id',$this->cmt_id)->
               get(['id','unit'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['unit'];
        }
        return $ret;
    }
    public function getSexOptions() {
        return CmnDefineNoun::where('cmn_define_id',2)
               ->lists('noun','id');
    }
    public function getRegAreaIdOptions() {
        $res = CmnArea::get(['id','area'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['area'];
        }
        return $ret;
    }
    public function getRegZoneIdOptions() {
        $res = CmnAreaZone::where('cmn_area_id',$this->reg_area_id)->
               get(['id','zip_code','zone'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['zip_code'].' '.$value['zone'];
        }
        return $ret;
    }
    public function getConAreaIdOptions() {
        $res = CmnArea::get(['id','area'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['area'];
        }
        return $ret;
    }
    public function getConZoneIdOptions() {
        $res = CmnAreaZone::where('cmn_area_id',$this->con_area_id)->
               get(['id','zip_code','zone'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['zip_code'].' '.$value['zone'];
        }
        return $ret;
    }
    public function getLivingStatusIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',11)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}
