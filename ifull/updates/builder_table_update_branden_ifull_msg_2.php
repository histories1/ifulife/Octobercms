<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsg2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dateTime('end_at');
            $table->smallInteger('cmt_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dropColumn('end_at');
            $table->dropColumn('cmt_id');
        });
    }
}
