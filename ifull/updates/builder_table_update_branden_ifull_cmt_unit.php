<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtUnit extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_unit', 'branden_ifull_cmt_unit');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_unit', 'branden_ifull_community_unit');
    }
}
