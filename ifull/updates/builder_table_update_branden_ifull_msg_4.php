<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsg4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->integer('msg_lazy_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dropColumn('msg_lazy_id');
        });
    }
}
