<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtUnit2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_unit', function($table)
        {
            $table->renameColumn('cmn_floor_id', 'floor_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_unit', function($table)
        {
            $table->renameColumn('floor_id', 'cmn_floor_id');
        });
    }
}
