<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullmsgblock extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg_block', function($table)
        {
            $table->renameColumn('block_id', 'cmt_block_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg_block', function($table)
        {
            $table->renameColumn('cmt_block_id', 'block_id');
        });
    }
}
