<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmt2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('community', 'cmt');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt', function($table)
        {
            $table->renameColumn('cmt', 'community');
        });
    }
}
