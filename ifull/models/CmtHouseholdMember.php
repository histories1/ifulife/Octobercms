<?php namespace Branden\iFull\Models;

use Model;
/**
 * Model
 */
class CmtHouseholdMember extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_household_member';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /* ListValue */
    public $hasOne =[
        'sexs' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'sex'],
    ];
    /* Dropdown */
    public function getSexOptions() {
        return CmnDefineNoun::where('cmn_define_id',2)
               ->lists('noun','id');
    }
}
