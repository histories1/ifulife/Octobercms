<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullAreaZone extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_area_zone', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->smallInteger('area_id')->unsigned();
            $table->text('zip_code');
            $table->text('zone');
            $table->boolean('is_activated')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_area_zone');
    }
}
