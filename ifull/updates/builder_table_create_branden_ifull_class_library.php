<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullClassLibrary extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_class_library', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('cmt_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->text('name');
            $table->text('purpose')->nullable();
            $table->integer('hours')->nullable()->unsigned();
            $table->text('restrict')->nullable();
            $table->text('tuition');
            $table->text('other_charge')->nullable();
            $table->text('remark')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_class_library');
    }
}
