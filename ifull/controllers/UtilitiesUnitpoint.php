<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use Backend;
use Redirect;

class UtilitiesUnitpoint extends Controller
{
  public $implement = [
    'Backend\Behaviors\ListController',
    'Backend\Behaviors\FormController'
  ];

  public $listConfig = 'config_list.yaml';
  public $formConfig = 'config_form.yaml';


  public function __construct()
  {
    parent::__construct();
    BackendMenu::setContext('Branden.iFull', 'main-menu-item7', 'side-menu-item4');
  }


  public function index()
  {
    // Call the ListController behavior index() method
    $this->asExtension('ListController')->index();
    // var_dump($this->widget->list);

  }


  /**
   * 處理配發住戶公設時段設定流程
   * octobercms的model相關API
   * @see https://octobercms.com/docs/database/collection
   *
   * @param int $recordId 公設編號
   *
   * @return void
   */
  public function phase($recordId, $context = null)
  {

    // 傳送json 定義參數
    $this->vars['actives'] = $actives;
    $this->vars['recordId'] = $recordId;
    $this->vars['cols'] = $this->week_cols;
    $this->vars['rows'] = $this->_setSectionRows();
  }


  /**
   * 處理更新公設時段
   *
   * 目前作法：先全部清除再重建整批數值
   *
   * @param string _session
   * @param string _token
   *
   * @return json [
   *  #layout-flash-messages,
   *  X_OCTOBER_ASSETS[
   *      js,
   *      css,
   *      rss
   *  ]
   * ]
   */
  public function onSave()
  {
    // 取得POST資料並格式化
    $postDatas = $_POST;

    $sectionLabels = $this->_setSectionRows();

    $utilities = \Branden\iFull\Models\Utilities::find($recordId);
    // var_dump($utilities->utilities);
    // die();
    // echo $sectionLabels[0]."<BR/>";


    // AJAX回應作法！
    Flash::success("已配置[$utilities->utilities]開放時段");
    if (!empty($postDatas['close'])) {
      // return Backend::redirect('branden/ifull/utilitiesactive');
    }
  }
}
