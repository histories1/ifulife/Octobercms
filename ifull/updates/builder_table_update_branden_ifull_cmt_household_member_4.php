<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtHouseholdMember4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->string('password', 15)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmt_household_member', function($table)
        {
            $table->dropColumn('password');
        });
    }
}
