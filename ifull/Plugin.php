<?php namespace Branden\iFull;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Branden\Ifull\Components\Msgs' => 'msgs'
        ];
    }

    public function registerSettings()
    {
    }
}
