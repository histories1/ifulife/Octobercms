<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCmnParkType extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_cmn_park_type');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_cmn_park_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('park_type', 20);
            $table->boolean('is_motor')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
