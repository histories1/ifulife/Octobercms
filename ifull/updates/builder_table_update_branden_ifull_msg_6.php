<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMsg6 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->boolean('is_active')->default(0);
            $table->boolean('is_ontop')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_msg', function($table)
        {
            $table->dropColumn('is_active');
            $table->dropColumn('is_ontop');
        });
    }
}
