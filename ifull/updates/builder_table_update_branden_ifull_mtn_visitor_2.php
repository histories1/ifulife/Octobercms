<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMtnVisitor2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mtn_visitor', function($table)
        {
            $table->integer('cmt_id')->unsigned();
            $table->integer('cmt_household_member_id')->unsigned();
            $table->integer('cmt_unit_id')->unsigned();
            $table->dropColumn('community_id');
            $table->dropColumn('community_household_member_id');
            $table->dropColumn('community_unit_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mtn_visitor', function($table)
        {
            $table->dropColumn('cmt_id');
            $table->dropColumn('cmt_household_member_id');
            $table->dropColumn('cmt_unit_id');
            $table->integer('community_id')->unsigned();
            $table->integer('community_household_member_id')->unsigned();
            $table->integer('community_unit_id')->unsigned();
        });
    }
}
