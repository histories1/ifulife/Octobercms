<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmtHouseholdEmergency extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_household_emergency';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
