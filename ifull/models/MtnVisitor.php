<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;
use DateTime;

use October\Rain\Database\Traits\Nullable;

/**
 * Model
 */
class MtnVisitor extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_mtn_visitor';

    use Nullable;

    public $nullable = [
        'cmt_id',    // Define which fields should be inserted as NULL when
        'cmt_unit_id',
        'cmt_household_member_id',
        'phone',
        'visitor_count',
        'in_at',
        'out_at',
        'remark',
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->setCmtIDAttribute();
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');
        }
        $this->attributes['cmt_id'] = $cmtid;
    }

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtunit' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'cmt_unit_id'],
        'cmthouseholdmember' => [
            CmtHouseholdMember::class,
            'key' => 'id',
            'otherKey' => 'cmt_household_member_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $res = Cmt::get(['id','community'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $res = CmtUnit::where('cmt_id',$this->cmt_id)->
               get(['id','unit'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['unit'];
        }
        return $ret;
    }
    public function getCmtHouseholdMemberIdOptions() {
        $cmthouseholdid = CmtHousehold::
                          where('cmt_unit_id',$this->cmt_unit_id)
                          ->value('id');
        return CmtHouseholdMember::where('cmt_household_id',$cmthouseholdid)
               ->lists('name','id');
    }
    /*選取領件人->自動帶入領件時間*/
    public function filterFields($fields, $context = null)
    {
        if (empty($this->cmt_unit_id))
            return;
        else
        {
            $datetime = new DateTime('now');
            $fields->in_at->value = $datetime;
        }
    }
}
