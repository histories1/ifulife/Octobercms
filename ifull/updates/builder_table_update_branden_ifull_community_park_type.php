<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityParkType extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_park_type', function($table)
        {
            $table->boolean('is_motor')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_park_type', function($table)
        {
            $table->dropColumn('is_motor');
        });
    }
}
