<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilitiesBooking extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities_booking', function($table)
        {
            $table->boolean('is_come')->default(0);
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities_booking', function($table)
        {
            $table->dropColumn('is_come');
        });
    }
}
