<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilitiesActive3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities_active', function($table)
        {
            $table->renameColumn('week1', 'week');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities_active', function($table)
        {
            $table->renameColumn('week', 'week1');
        });
    }
}
