<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullArea extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_area', function($table)
        {
            $table->string('area_code', 10)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('area', 30)->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_area', function($table)
        {
            $table->text('area_code')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('area')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
