<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity5 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->decimal('ld_arcade_area', 10, 2)->change();
            $table->decimal('ld_setback_area', 10, 2)->change();
            $table->decimal('ld_other_area', 10, 2)->change();
            $table->decimal('ld_total_area', 10, 2)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->decimal('ld_arcade_area', 10, 0)->change();
            $table->decimal('ld_setback_area', 10, 0)->change();
            $table->decimal('ld_other_area', 10, 0)->change();
            $table->decimal('ld_total_area', 10, 0)->change();
        });
    }
}
