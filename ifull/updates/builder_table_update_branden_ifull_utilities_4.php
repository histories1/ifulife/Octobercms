<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilities4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->integer('point')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->dropColumn('point');
        });
    }
}
