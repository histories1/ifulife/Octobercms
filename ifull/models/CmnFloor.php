<?php namespace Branden\iFull\Models;

use Model;

/**
 * Model
 */
class CmnFloor extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmn_floor';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
