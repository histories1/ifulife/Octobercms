<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

use October\Rain\Database\Traits\Nullable; 

/**
 * Model
 */
class CmtUnit extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_cmt_unit';

    use Nullable;

    public $nullable = [
        'cmn_area_id',    // Define which fields should be inserted as NULL when 
        'cmn_zone_id',    // Define which fields should be inserted as NULL when 
        'cmt_block_id'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];    

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
        $this->attributes['cmt_block_id'] = $this->exists ? $this->cmt_block_id : post('cmt_block_id'); 
    }
    /* Relations */
    public $hasMany =[
        'cmtparks' => [
            //'branden\ifull\models\CmtPark',
            CmtPark::class,
            'table' => 'branden_ifull_cmt_park'//,
            //'order' => 'block'
        ]
    ];

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtblock' => [
            CmtBlock::class,
            'key' => 'id',
            'otherKey' => 'cmt_block_id'],
        'cmnarea' => [
            CmnArea::class,
            'key' => 'id',
            'otherKey' => 'cmn_area_id'],    
        'cmnareazone' => [
            CmnAreaZone::class,
            'key' => 'id',
            'otherKey' => 'cmn_zone_id'],
        'floor' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'floor_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $res = Cmt::get(['id','community'])->toArray();
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }     
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtBlockIdOptions() {
        return CmtBlock::where('cmt_id',$this->cmt_id)->lists('block','id');
    }
    public function getCmnAreaIdOptions() {
        $res = CmnArea::get(['id','area'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['area'];
        }
        return $ret;
    }
    public function getCmnZoneIdOptions() {
        $res = CmnAreaZone::where('cmn_area_id',$this->cmn_area_id)->get(['id','zip_code','zone'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['zip_code'].' '.$value['zone'];
        }
        return $ret;
    }
    public function getFloorIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',15)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
}
