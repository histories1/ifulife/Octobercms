<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->decimal('coverage_ratio', 10, 2)->change();
            $table->decimal('area_ratio', 10, 2)->change();
            $table->decimal('bd_svl_area', 10, 2)->change();
            $table->decimal('bd_total_area', 10, 2)->change();
            $table->decimal('bd_height', 10, 2)->change();
            $table->decimal('bd_arcade_area', 10, 2)->change();
            $table->decimal('bd_other_area', 10, 2)->change();
            $table->decimal('above_ars_area', 10, 2)->change();
            $table->decimal('under_ars_area', 10, 2)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->decimal('coverage_ratio', 10, 0)->change();
            $table->decimal('area_ratio', 10, 0)->change();
            $table->decimal('bd_svl_area', 10, 0)->change();
            $table->decimal('bd_total_area', 10, 0)->change();
            $table->decimal('bd_height', 10, 0)->change();
            $table->decimal('bd_arcade_area', 10, 0)->change();
            $table->decimal('bd_other_area', 10, 0)->change();
            $table->decimal('above_ars_area', 10, 0)->change();
            $table->decimal('under_ars_area', 10, 0)->change();
        });
    }
}
