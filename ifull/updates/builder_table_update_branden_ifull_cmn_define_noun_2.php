<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnDefineNoun2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_define_noun', function($table)
        {
            $table->string('snoun', 30)->nullable();
            $table->string('noun', 50)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_define_noun', function($table)
        {
            $table->dropColumn('snoun');
            $table->string('noun', 30)->change();
        });
    }
}
