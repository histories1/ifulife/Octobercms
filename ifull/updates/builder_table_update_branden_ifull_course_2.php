<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourse2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->integer('course_type_id')->unsigned();
            $table->integer('course_library_id')->unsigned();
            $table->dropColumn('course_type_id');
            $table->dropColumn('course_library_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->dropColumn('course_type_id');
            $table->dropColumn('course_library_id');
            $table->integer('course_type_id')->unsigned();
            $table->integer('course_library_id')->unsigned();
        });
    }
}
