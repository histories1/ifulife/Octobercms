<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnMsgType2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_cmn_msg_type', function($table)
        {
            $table->string('msg_type_sname', 20)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_cmn_msg_type', function($table)
        {
            $table->dropColumn('msg_type_sname');
        });
    }
}
