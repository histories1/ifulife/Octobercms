<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullOpinionReply extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_opinion_reply', function($table)
        {
            $table->integer('backend_users_id');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_opinion_reply', function($table)
        {
            $table->dropColumn('backend_users_id');
        });
    }
}
