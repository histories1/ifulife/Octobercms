<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;
use DateTime;

use October\Rain\Database\Traits\Nullable;

/**
 * Model
 */
class Mpg extends Model
{
    use \October\Rain\Database\Traits\Validation;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_mpg';

    use Nullable;

    public $nullable = [
        'cmt_id',
        'cmt_unit_id',
        'cmt_household_member_id',
        'mpg_type_id',
        'mpg_location_id',
        'rcp_cmt_household_member_id',
        'logistics_id',
        'return_logistics_id'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {
        parent::__construct();
        $this->setCmtIDAttribute();
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');
        }
        $this->attributes['cmt_id'] = $cmtid;
    }

    /**
     * @var array Relations
     */
    public $hasMany = [
        'mpgstatus' => ['Branden\Ifull\Models\MpgStatus']
    ];
    /* Relations
    public $hasMany =[
        'mpgstatus' => [
            //'branden\ifull\models\CmtBlock',
            MpgStatus::class,
            'table' => 'branden_ifull_mpg_status'//,
            //'order' => 'block'
        ],
    ];*/

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id'],
        'cmtunit' => [
            CmtUnit::class,
            'key' => 'id',
            'otherKey' => 'cmt_unit_id'],
        'cmthouseholdmember' => [
            CmtHouseholdMember::class,
            'key' => 'id',
            'otherKey' => 'cmt_household_member_id'],
        'mpgtype' => [
            MpgType::class,
            'key' => 'id',
            'otherKey' => 'mpg_type_id'],
        'mpglocation' => [
            MpgLocation::class,
            'key' => 'id',
            'otherKey' => 'mpg_location_id'],
        'rcpcmthouseholdmember' => [
            CmtHouseholdMember::class,
            'key' => 'id',
            'otherKey' => 'rcp_cmt_household_member_id'],
        'statusid' => [
            CmnDefineNoun::class,
            'key' => 'id',
            'otherKey' => 'status']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            $res = Cmt::get(['id','community'])->toArray();
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmtUnitIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
            return CmtUnit::where('cmt_id',$this->cmt_id)
                   ->lists('unit','id');
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            return CmtUnit::where('cmt_id',$cmtid)
                   ->lists('unit','id');
        }
    }
/*
    public function getCmtUnitIdOptions() {
        $res = CmtUnit::where('cmt_id',$this->cmt_id)->
               get(['id','unit'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['unit'];
        }
        return $ret;
    }
*/
    public function getCmtHouseholdMemberIdOptions() {
        $cmthouseholdid = CmtHousehold::
                          where('cmt_unit_id',$this->cmt_unit_id)
                          ->value('id');
        return CmtHouseholdMember::where('cmt_household_id',$cmthouseholdid)
               ->lists('name','id');
    }
    public function getMpgTypeIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
             return MpgType::where('cmt_id',$this->cmt_id)
                    ->lists('mpg_type','id');
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            return MpgType::where('cmt_id',$cmtid)
                    ->lists('mpg_type','id');
        }
    }
    public function getMpgLocationIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {
             return MpgLocation::where('cmt_id',$this->cmt_id)
                    ->lists('location','id');
        }
        else
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            return MpgLocation::where('cmt_id',$cmtid)
                    ->lists('location','id');
        }
    }
    public function getRcpCmtHouseholdMemberIdOptions() {
        $cmthouseholdid = CmtHousehold::
                          where('cmt_unit_id',$this->cmt_unit_id)
                          ->value('id');
        return CmtHouseholdMember::where('cmt_household_id',$cmthouseholdid)
               ->lists('name','id');
    }
    public function getStatusOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',20)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getLogisticsIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',18)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    public function getReturnLogisticsIdOptions() {
        $res = CmnDefineNoun::where('cmn_define_id',18)->get(['id','noun'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['noun'];
        }
        return $ret;
    }
    /*選取領件人->自動帶入領件時間*/
    public function filterFields($fields, $context = null)
    {
        if (empty($this->rcp_cmt_household_member_id))
            return;
        else
        {
            $datetime = new DateTime('now');
            $fields->collar_at->value = $datetime;
            $fields->status   ->value = 123;
        }
    }
    /*顯示私人領件*/
    public static function getIsPrivateOptions() {
        return[
          '0'=>'',
          '1'=>'私人領件'
          ];
    }
    public function getIsPrivateAttribute()
    {
        $result = $this->attributes['is_private'];
        $options = $this->getIsPrivateOptions();
        foreach($options as $key=>$value)
        {
            if($key == $result)
            {
                return $value;
            }
        }
    }
}
