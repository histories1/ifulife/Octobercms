<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnOpinionType extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_opinion_type', 'branden_ifull_cmn_opinion_type');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_opinion_type', 'branden_ifull_opinion_type');
    }
}
