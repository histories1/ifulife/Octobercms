<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunity extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community', function($table)
        {
            $table->dropColumn('deleted_at');
        });
    }
}
