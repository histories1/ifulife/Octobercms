<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullUtilitiesActive extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_utilities_active', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('utilities_id')->unsigned();
            $table->integer('week');
            $table->boolean('c00_active')->default(0);
            $table->integer('c00_point')->nullable();
            $table->boolean('c01_active')->default(0);
            $table->integer('c01_point')->nullable();
            $table->boolean('c02_active')->default(0);
            $table->integer('c02_point')->nullable();
            $table->boolean('c03_active')->default(0);
            $table->integer('c03_point')->nullable();
            $table->boolean('c04_active')->default(0);
            $table->integer('c04_point')->nullable();
            $table->boolean('c05_active')->default(0);
            $table->integer('c05_point')->nullable();
            $table->boolean('c06_active')->default(0);
            $table->integer('c06_point')->nullable();
            $table->boolean('c07_active')->default(0);
            $table->integer('c07_point')->nullable();
            $table->boolean('c08_active')->default(0);
            $table->integer('c08_point')->nullable();
            $table->boolean('c09_active')->default(0);
            $table->integer('c09_point')->nullable();
            $table->boolean('c10_active')->default(0);
            $table->integer('c10_point')->nullable();
            $table->boolean('c11_active')->default(0);
            $table->integer('c11_point')->nullable();
            $table->boolean('c12_active')->default(0);
            $table->integer('c12_point')->nullable();
            $table->boolean('c13_active')->default(0);
            $table->integer('c13_point')->nullable();
            $table->boolean('c14_active')->default(0);
            $table->integer('c14_point')->nullable();
            $table->boolean('c15_active')->default(0);
            $table->integer('c15_point')->nullable();
            $table->boolean('c16_active')->default(0);
            $table->integer('c16_point')->nullable();
            $table->boolean('c17_active')->default(0);
            $table->integer('c17_point')->nullable();
            $table->boolean('c18_active')->default(0);
            $table->integer('c18_point')->nullable();
            $table->boolean('c19_active')->default(0);
            $table->integer('c19_point')->nullable();
            $table->boolean('c20_active')->default(0);
            $table->integer('c20_point')->nullable();
            $table->boolean('c21_active')->default(0);
            $table->integer('c21_point')->nullable();
            $table->boolean('c22_active')->default(0);
            $table->integer('c22_point')->nullable();
            $table->boolean('c23_active')->default(0);
            $table->integer('c23_point')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_utilities_active');
    }
}
