<?php namespace Branden\iFull\Models;

use Model;
use BackendAuth;
use DB;

/**
 * Model
 */
class MsgLazy extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'branden_ifull_msg_lazy';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /*Initial*/
    public function __construct(array $attributes = [])
    {        
        parent::__construct();
        $this->setCmtIDAttribute();        
    }
    public function setCmtIDAttribute()
    {

        $cmtid = $this->exists ? $this->cmt_id : post('cmt_id');  
        if ($cmtid == null)
        {
            $user  = BackendAuth::getUser();
            $uid   = $user ->id;
            $cmtid = DB::table('branden_ifull_cmt_backend_users')
                     ->where('backend_users_id',$uid)
                     ->value('cmt_id');            
        }        
        $this->attributes['cmt_id'] = $cmtid;
    }

    /* ListValue */
    public $hasOne =[
        'cmt' => [
            Cmt::class,
            'key' => 'id',
            'otherKey' => 'cmt_id' ],
        'cmnmsgtype' => [
            CmnMsgType::class,
            'key' => 'id',
            'otherKey' => 'cmn_msg_type_id']
    ];

    /* Dropdown */
    public function getCmtIdOptions() {
        $user   = BackendAuth::getUser();
        $uid    = $user ->id;
        $useall = DB::table('branden_ifull_cmt_backend_users')
                  ->where('backend_users_id',$uid)
                  ->where('cmt_id',1)
                  ->value('cmt_id');
        if ($useall == 1)
        {         
            $res = Cmt::get(['id','community'])->toArray();
        }
        else 
        {
            $cmtid  = DB::table('branden_ifull_cmt_backend_users')
                      ->where('backend_users_id',$uid)
                      ->lists('cmt_id');
            $res = Cmt::wherein('id', $cmtid)
                   ->get(['id','community'])->toArray();
        }     
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['community'];
        }
        return $ret;
    }
    public function getCmnMsgTypeIdOptions() {
        $res = CmnMsgType::get(['id','msg_type'])->toArray();
        $ret = [];
        foreach($res as $value) {
            $ret[$value['id']] = $value['msg_type'];
        }
        return $ret;
    }
}
