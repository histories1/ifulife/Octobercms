<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpg3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->text('remark');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mpg', function($table)
        {
            $table->dropColumn('remark');
        });
    }
}
