<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityBlockUnit4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->integer('area_id')->nullable()->unsigned();
            $table->integer('zone_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->dropColumn('area_id');
            $table->dropColumn('zone_id');
        });
    }
}
