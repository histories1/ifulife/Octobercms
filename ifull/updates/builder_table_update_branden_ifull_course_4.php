<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourse4 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->integer('enrollment_status')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_course', function($table)
        {
            $table->dropColumn('enrollment_status');
        });
    }
}
