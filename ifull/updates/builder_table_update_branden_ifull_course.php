<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCourse extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_class', 'branden_ifull_course');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_course', 'branden_ifull_class');
    }
}
