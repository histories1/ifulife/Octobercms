<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCmnLivingStatus extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_cmn_living_status');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_cmn_living_status', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('living_status', 30);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
