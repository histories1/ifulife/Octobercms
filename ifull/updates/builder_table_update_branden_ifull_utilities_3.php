<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilities3 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->text('rule');
            $table->boolean('is_active')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->dropColumn('rule');
            $table->boolean('is_active')->default(null)->change();
        });
    }
}
