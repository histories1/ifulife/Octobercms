<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullGas2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->integer('month')->unsigned();
            $table->renameColumn('year_month', 'year');
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_gas', function($table)
        {
            $table->dropColumn('month');
            $table->renameColumn('year', 'year_month');
        });
    }
}
