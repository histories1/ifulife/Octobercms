<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmtBlock extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_block', 'branden_ifull_cmt_block');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmt_block', 'branden_ifull_community_block');
    }
}
