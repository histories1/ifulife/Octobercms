<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullUtilities extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->integer('cmt_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_utilities', function($table)
        {
            $table->dropColumn('cmt_id');
        });
    }
}
