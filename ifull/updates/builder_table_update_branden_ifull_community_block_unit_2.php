<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCommunityBlockUnit2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->integer('floor_id')->nullable()->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_community_block_unit', function($table)
        {
            $table->dropColumn('floor_id');
        });
    }
}
