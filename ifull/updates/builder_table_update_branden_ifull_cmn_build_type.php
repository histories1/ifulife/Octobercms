<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullCmnBuildType extends Migration
{
    public function up()
    {
        Schema::rename('branden_ifull_community_build_type', 'branden_ifull_cmn_build_type');
    }
    
    public function down()
    {
        Schema::rename('branden_ifull_cmn_build_type', 'branden_ifull_community_build_type');
    }
}
