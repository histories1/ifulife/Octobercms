<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteBrandenIfullCmnUseZone extends Migration
{
    public function up()
    {
        Schema::dropIfExists('branden_ifull_cmn_use_zone');
    }
    
    public function down()
    {
        Schema::create('branden_ifull_cmn_use_zone', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('use_zone', 20);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
