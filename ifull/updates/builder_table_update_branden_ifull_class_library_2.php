<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullClassLibrary2 extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_class_library', function($table)
        {
            $table->string('name', 100)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('purpose', 100)->nullable()->unsigned(false)->default(null)->change();
            $table->string('hours', 100)->nullable()->unsigned(false)->default(null)->change();
            $table->string('restrict', 100)->nullable()->unsigned(false)->default(null)->change();
            $table->string('tuition', 100)->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('other_charge', 100)->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_class_library', function($table)
        {
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('purpose')->nullable()->unsigned(false)->default(null)->change();
            $table->integer('hours')->nullable()->unsigned()->default(null)->change();
            $table->text('restrict')->nullable()->unsigned(false)->default(null)->change();
            $table->text('tuition')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('other_charge')->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
