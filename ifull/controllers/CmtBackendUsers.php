<?php namespace Branden\iFull\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CmtBackendUsers extends Controller
{
    public $implement = [        
    	'Backend\Behaviors\ListController',        
    	'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Branden.iFull', 'main-menu-item8', 'side-menu-item');
    }
}
