<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrandenIfullmsgblock extends Migration
{
    public function up()
    {
        Schema::create('branden_ifull_msg_block', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('msg_id')->unsigned();
            $table->integer('cmt_id')->unsigned();
            $table->integer('block_id')->unsigned();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('branden_ifull_msg_block');
    }
}
