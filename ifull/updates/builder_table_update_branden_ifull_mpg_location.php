<?php namespace Branden\iFull\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrandenIfullMpgLocation extends Migration
{
    public function up()
    {
        Schema::table('branden_ifull_mpg_location', function($table)
        {
            $table->integer('community_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::table('branden_ifull_mpg_location', function($table)
        {
            $table->dropColumn('community_id');
        });
    }
}
